const bcrypt = require('bcrypt-nodejs');
const CustomError = require('../helpers/CustomError');

module.exports = {
  findByFilter(filter, cb) {
    User.find().exec((err, users) => {
      if (err) return cb(err);
      if (users.length === 0) return cb(new CustomError('There are no users for your filter', 404));

      return cb(null, users);
    })
  },

  findById(id, cb) {
    User.findOneBy_id(id).exec((err, data) => {
      if (err) return cb(err);
      if (!data) return cb(new CustomError('There are no such user', 404));

      return cb(null, data);
    });
    },

  create(newUser, cb) {
    bcrypt.hash(newUser.password, null, null, (err, hash) => {
      let missingFields = checkDataKeysValid(newUser, User.allowedKeys, User.expectedKeys);
      if (missingFields.length !== 0) return cb(new CustomError(`Missing Field: ${missingFields}`));

      newUser.hashedPassword = hash;
      if (newUser.password) delete newUser.password;

      User.create(newUser).exec((err, user) => {
        if (err) {
          return cb(err);
        }
        return cb(null, {user});
      });
    });
  },

  update(user, cb) {
    let id = user.id;
    deleteNotAlowedKeys(user, User.allowedKeys);
    if (user.password) {
      // TODO: rebuild with async hash implementation
      user.hashedPassword = bcrypt.hashSync(user.password, null);
    }
    User.findOne({_id: id}).exec((err, user) => {
      if (err) return cb(err);
      if (!user) return cb(new CustomError('There are no such user'));

      User.update({_id: id}, user, (err, updated) => {
        if (err) return cb(err);

        return cb(null, {user: updated[0]});
      });
    });
  },

  delete(user, cb) {
    User.destroy({_id: user.id}).exec((err) => {
      if (err) {
        return cb(err);
      }

      return cb(null);
    })
  }
};

function deleteNotAlowedKeys(user, allowedKeys) {
  for (let key in user) {
    if (!_.contains(allowedKeys, key)) delete user[key];
  }
}

function checkDataKeysValid(user, allowedKeys, expectedKeys) {
  deleteNotAlowedKeys(user, allowedKeys);

  return _.difference(expectedKeys, Object.keys(user));

}
