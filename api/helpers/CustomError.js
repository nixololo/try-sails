class CustomError {
  constructor(message, status=400) {
    this._message = message;
    this._status = status;
  }

  get message() {
    return this._message;
  }

  get status() {
    return this._status;
  }

  toString() {
    return `User's data error. Message: ${message}`;
  }
}

module.exports = CustomError;
