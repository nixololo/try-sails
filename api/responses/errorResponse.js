module.exports = function(error) {
  const req = this.req;
  const res = this.res;

  res.statusCode = 500;
  if (error.constructor.name === 'CustomError') res.statusCode = error.status;

  res.json({error: {message: error.message}});
};
