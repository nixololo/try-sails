/**
 * User.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    username: {
      type: 'string',
      unique: true,
      primaryKey: true,
      required: true
    },
    firstName: {
      type: 'string',
    },
    lastName: {
      type: 'string',
    },
    hashedPassword: {
      type: 'string',
      required: true
    },
    age: {
      type: 'integer'
    },
    currentPoints: {
      type: 'integer',
      defaultsTo: 0
    },
    _id: {
      type: 'objectId',
    }
  },

  /**
   * Kostil Time with Radion
   * @returns {Array}
   */
  get allowedKeys() {
      if (!this._allowedKeys) {
        this._allowedKyes = ['password'];
        for (let key in this.attributes) {
          if (this.attributes.hasOwnProperty(key) && key !== '_id' && key !== 'hashedPassword') {
            this._allowedKyes.push(key);
          }
        }
      }

      return this._allowedKyes;
  },

  get expectedKeys() {
    if (!this._expectedKeys) {
      this._expectedKeys = ['password'];
      for (let key in this.attributes) {
        if (
            !!this.attributes[key].required &&
            this.attributes.hasOwnProperty(key) &&
            key !== '_id' &&
            key !== 'hashedPassword'
        ) {
          this._expectedKeys.push(key);
        }
      }
    }

    return this._expectedKeys;
  }

};

