/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

  get(req, res) {
    let filter = req.allParams();
    console.log(filter);
    UserService.findByFilter(filter, (err, users) => {
      if (err) {
        sails.log.error(err);
        return res.errorResponse(err);
      }

      return res.json({count: users.length, users: users});
    });
  },

  getById(req, res) {
    let id = req.allParams().id;

    UserService.findById(id, (err, user) => {
      if (err) {
        sails.log.error(err);
        return res.errorResponse(err);
      }

      return res.json({user});
    });
  },

	create(req, res) {
	  UserService.create(req.allParams(), (err, data) => {
      if (err) {
        sails.log.error(err);
        return res.errorResponse(err);
      }

	    res.statusCode = 201;
      return res.json({user: {id: data.user.id}});
    })
  },

  update(req, res) {
    let updatesForUser = req.allParams();
    UserService.update(updatesForUser, (err, updatedUser) => {
      if (err) {
        sails.log.error(err);
        return res.errorResponse(err);
      }

      return res.json(updatedUser);
    })
  },

  delete(req, res) {
    let user = {
      id: req.allParams().id
    };

    UserService.delete(user, (err) => {
      if (err) {
        sails.log.error(err);
        return res.errorResponse(err);
      }

      res.type('json');
      return res.ok();
    });
  }
};
