const request = require('supertest');

describe('UserController', () => {
  let incorrectId = '1231sdfa1';
  let correctId;
  let incorrectNewUser = {
    username: 'nixololo',
    firstName: 'Radion',
    lastName: 'Cheriakov',
    age: 45,
  };
  let correctNewUser = {
    username: 'nixololo',
    firstName: 'Radion',
    lastName: 'Cheriakov',
    age: 45,
    password: '147258aa'
  };
  let userUpdates = {
    username: 'some another username',
    age: 20
  };

  describe('#create() incorrect user', () => {
    it('it should not POST user', (done) => {
      request(sails.hooks.http.app)
        .post('/users')
        .send(incorrectNewUser)
        .expect('Content-Type', /json/)
        .expect(400, done);
    });
  });

  describe('#create() correct user', () => {
    it('it should POST user', (done) => {
      request(sails.hooks.http.app)
        .post('/users')
        .send(correctNewUser)
        .expect('Content-Type', /json/)
        .expect(201)
        .end((err, res) => {
          if (err) throw err;

          correctId = res.body.user.id;
          done();
        });
    })
  });

  describe('#update() incorrent user', () => {
    it('it should not PUT user', (done) => {
      request(sails.hooks.http.app)
        .put(`/users/${incorrectId}`)
        .send({username: 'some user name'})
        .expect('Content-Type', /json/)
        .expect(400, done);
    });
  });

  describe('#update() correct user', () => {
    it('it should PUT user', (done) => {
      request(sails.hooks.http.app)
        .put(`/users/${correctId}`)
        .send(userUpdates)
        .expect('Content-Type', /json/)
        .expect(200, done)
    });
  });

  describe('#getById() by incorrect id', () => {
    it('it should not GET the user', (done) => {
      request(sails.hooks.http.app)
        .get(`/users/${incorrectId}`)
        .expect('Content-Type', /json/)
        .expect(404, done);
    });
  });

  describe('#getById() by correct id', () => {
    it('it should GET the user', (done) => {
      request(sails.hooks.http.app)
        .get(`/users/${correctId}`)
        .expect('Content-Type', /json/)
        .expect(200, done);
    });
  });

  describe('#delete() by correct id', () => {
    it('it should DELETE the user', (done) => {
      request(sails.hooks.http.app)
        .delete(`/users/${correctId}`)
        .expect('Content-Type', /json/)
        .expect(200, done);
    });
  });

  describe('#getById() by currently deleted id', () => {
    it('it should not GET the user', (done) => {
      request(sails.hooks.http.app)
        .get(`/users/${correctId}`)
        .expect('Content-Type', /json/)
        .expect(404, done);
    });
  });

  describe('#delete() by currently deleted id', () => {
    it('it should response with 200', (done) => {
      request(sails.hooks.http.app)
        .delete(`/users/${correctId}`)
        .expect('Content-Type', /json/)
        .expect(200, done);
    });
  });

});


